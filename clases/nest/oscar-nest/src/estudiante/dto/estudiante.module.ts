import { Module, Controller } from "@nestjs/common";
import {EstudianteController} from './estudiante.controller'
import {EstudianteService} from './estudiante.service'

@Module({
    imports:[],
    controllers:[EstudianteController],
    providers:[EstudianteService]
})

export class EstudianteModulo  {
}