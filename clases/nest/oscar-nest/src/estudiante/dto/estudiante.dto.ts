//import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max, IsOptional } from "class-validator";

export class EstudianteDto {
    // @IsInt()
    // @IsOptional()
    id?: number;


    nombre: string;
    apellido: string;
    edad: number;
    cedula: string;
    esEgresado: boolean;
    fechaIngreso: Date;
}