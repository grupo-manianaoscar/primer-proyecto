import { Controller, Get, Param, Post, Put, Delete } from "@nestjs/common";
import { EstudianteService } from "./estudiante.service"
import { EstudianteDto } from './estudiante.dto'

@Controller('estudiante')

export class EstudianteController {
    //metodos http bucar todo, buscr un, crear , editar eliminar
    constructor(private readonly estudianteService: EstudianteService) {
        //los servicios 
    }

    @Get()
    obtenerEstudiantes(): string {
        return ` ${this.estudianteService.obtenerEstudiantes()}`
    }
    @Get(':id')
    obtenerEstudiante(@Param('id') id): string {
        return ` ${this.estudianteService.obtenerEstdudiante(id)}`
    }

    @Post()
    crearEstudiante() {
      
        return this.estudianteService.crearEstudiante({ nombre: 'oscar' })
    }

    @Put(':id')
    editarEstudiante(@Param('id') id) {
        return this.estudianteService.editarEstudiante(id)
    }

    @Delete(':id')
    eliminarEstudiante(@Param('id') id) {
        return this.estudianteService.eliminarEstudiante(id)

    }

}
