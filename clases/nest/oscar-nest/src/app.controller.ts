import { Get, Controller, Param, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { createReadStream } from 'fs';

@Controller('inicio')//se manda el nombre de la ruta
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get(':id/nombre/:nombre/apellido/:apellido/correo/:correo') 
  obtenerUno(@Param('id')idUsuario, @Param('nombre') nombre, @Param('apellido') apellido, @Param('correo') correo ): string{
    return `Hola   ${this.appService.obtenerUno(idUsuario, nombre, apellido, correo)} `
  }
  
  @Post()
  crear(@Param('nombre') nombre):string{
    return this.appService.crear({nombre:`${nombre}`})
  }
  @Get()
  obtenerTodos(): string{
    return `Hola ${this.appService.obtenerTodos()}`
  }
  // root(@Param('nombre') nombreUsuario): string{
  //   const nombre= nombreUsuario
  //   return `hola ${nombre}`
  // }
  // root(): string {
  //   return '<h1> titulo </h1>';
  // }
  // root(): object {
  //   return {nombre: 'Oscar'};
  // }
  // root(): number[] {
  //   return [2,3,4];
  // }
}
