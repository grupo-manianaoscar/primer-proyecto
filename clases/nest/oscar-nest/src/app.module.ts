import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EstudianteModulo } from 'estudiante/dto/estudiante.module';

@Module({
  imports: [EstudianteModulo],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
