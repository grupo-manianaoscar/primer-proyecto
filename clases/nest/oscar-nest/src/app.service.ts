import { Injectable } from '@nestjs/common';
import { Observable, of } from 'rxjs';

@Injectable()
export class AppService {
  // root(): string {
  //   return 'Hello World!';
  // }

  obtenerTodos():string{
    return 'hola retorno todo'
  }

  obtenerUnoObservable(id: number):Observable <string>{
    return of (`hola retorno usuario con id: ${id}`)
  }
  obtenerUno(id: number, nombre: string, apellido: string, correo: string):string {
    return (`hola retorno usuario con id: ${id} ${nombre} ${apellido} ${correo}`)
  }
  crear(datosACrear: any) {
    return `voy a crear algo ${datosACrear.nombre}`
  }
}
