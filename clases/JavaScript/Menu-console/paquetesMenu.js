const readlineSync = require('readline-sync');
const menu = require('console-menu');
const metodosCallbacks = require('../callbacks/deber-callbacks/paquetes.js')
const metodosPromesas = require('../promesas/deber-promesas/paquetes.js')
const llenarArregloMenu = require('./llenarArregloMenu.js')
const ejecutarMenuCallbacks = require('./ejecutarMenuCallbacks')
const ejecutarMenuPrincipal = require('./ejecutarMenuPrincipal')
const ejecutarMenuPromesas= require('./ejecutarMenuPromesas')
const configurarJsonMenu = require('./configurarJsonMenu')

const arregloOperacionesUsuario = [
    'Crear Usuario', 
    'Buscar un solo Usuario',
    'Buscar y Crear Un usuario',
    'Eliminar Usuario',
    'Buscar todos los Usuarios',
    'Salir'
]

const arregloUsuarios = [
    { nombre: 'Jon' },
    { nombre: 'Eddard' },
    { nombre: 'Daenerys' },
    { nombre: 'Tyrion' },
    { nombre: 'Jame' },
    { nombre: 'Sansa' },
    { nombre: 'Arya' },
    { nombre: 'Cercei' },
    { nombre: 'Rahegar' },
    { nombre: 'Lyanna' },
    { nombre: 'Perruris' },
    { nombre: 'Pousito' }
]


module.exports = {
    arregloUsuarios,
    arregloOperacionesUsuario,
    readlineSync,
    menu,
    metodosCallbacks,
    metodosPromesas,
    llenarArregloMenu,
    ejecutarMenuCallbacks,
    ejecutarMenuPrincipal,
    ejecutarMenuPromesas,
    configurarJsonMenu
}