module.exports = () => {

    const metodosMenu = require('./paquetesMenu.js')
    const arregloOperacionesUsuario = metodosMenu.arregloOperacionesUsuario
    const arregloUsuarios = metodosMenu.arregloUsuarios
    const ejecutarConsola = () => {
        return metodosMenu.readlineSync.question('Ingrese usuario ');
    }

    return metodosMenu.menu(metodosMenu.llenarArregloMenu(arregloOperacionesUsuario), metodosMenu.configurarJsonMenu('Opciones Callbacks')).then(item => {
        let titulo = item.title
        switch (titulo) {
            case 'Crear Usuario':
                metodosMenu.metodosCallbacks.crearUsuario(arregloUsuarios, ejecutarConsola(), (resultado) => {
                    console.log(resultado.mensaje)
                })
                metodosMenu.ejecutarMenuCallbacks()

                break;
            case 'Buscar un solo Usuario':
                metodosMenu.metodosCallbacks.buscarUsuario(arregloUsuarios, ejecutarConsola(), (resultado) => {
                    console.log(resultado.mensaje)
                    console.log(resultado.arregloUsuariosEncontrados)
                })
                metodosMenu.ejecutarMenuCallbacks()

                break;
            case 'Buscar y Crear Un usuario':

                metodosMenu.metodosCallbacks.buscarCrearUsuario(arregloUsuarios, ejecutarConsola(), (resultado) => {
                    console.log(resultado.mensaje)
                    console.log(resultado.arregloUsuario)
                })
                metodosMenu.ejecutarMenuCallbacks()
                break;

            case 'Eliminar Usuario':
                metodosMenu.metodosCallbacks.eliminarUsuario(arregloUsuarios, ejecutarConsola(), (resultado) => {
                    console.log(resultado.mensaje)
                })
                metodosMenu.ejecutarMenuCallbacks()

                break;
            case 'Buscar todos los Usuarios':
                const expresion = metodosMenu.readlineSync.question('Ingrese expresion ');
                metodosMenu.metodosCallbacks.buscarPorExpresionRegular(arregloUsuarios, expresion, (resultado) => {
                    console.log(resultado.mensaje)
                    console.log(resultado.arregloUsuariosEncontrados)
                })
                metodosMenu.ejecutarMenuCallbacks()
                break

            case 'Salir':
                metodosMenu.ejecutarMenuPrincipal()
                break
            default:
                console.log('Opcion invalida');
        }
    })

}