module.exports = ejecutarMenu = () => {
    const metodosMenu = require('./paquetesMenu.js')
    

    const menuSeleccion = [
        { title: 'Callbacks' },
        { title: 'Promesas' },
        { title: 'Observables' },
    ]

    return metodosMenu.menu(menuSeleccion, metodosMenu.configurarJsonMenu('Menu Principal'))
        .then(item => {
            let titulo = item.title
            switch (titulo) {
                case 'Callbacks':
                    metodosMenu.ejecutarMenuCallbacks()
                    break;
                case 'Promesas':
                    metodosMenu.ejecutarMenuPromesas()
                    break;
                case 'Observables':
                    break;
                default:
                    console.log('Opcion invalida');
            }

        });
}