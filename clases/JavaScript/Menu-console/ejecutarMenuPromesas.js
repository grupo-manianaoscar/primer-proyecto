module.exports = () => {
    const metodosMenu = require('./paquetesMenu.js')
    const arregloOperacionesUsuario = metodosMenu.arregloOperacionesUsuario
    const arregloUsuarios = metodosMenu.arregloUsuarios
    const ejecutarConsola = () => {
        return metodosMenu.readlineSync.question('Ingrese usuario ');
    }
    return metodosMenu.menu(metodosMenu.llenarArregloMenu(arregloOperacionesUsuario), metodosMenu.configurarJsonMenu('Opciones Promesas')).then(item => {
        let titulo = item.title
        switch (titulo) {
            case 'Crear Usuario':
                metodosMenu.metodosPromesas.crearUsuario(arregloUsuarios, ejecutarConsola())
                    .then(respuestaResolveCrear => {
                        console.log(respuestaResolveCrear.mensaje, respuestaResolveCrear.arregloUsuarios)
                        metodosMenu.ejecutarMenuPromesas()
                    })
                    .catch(respuestaRejectCrear => {
                        console.log(respuestaRejectCrear.mensaje)
                        metodosMenu.ejecutarMenuPromesas()
                    })

                break;
            case 'Buscar un solo Usuario':
                metodosMenu.metodosPromesas.buscarUsuario(arregloUsuarios, ejecutarConsola())
                    .then(respuestaResolveBuscar => {
                        console.log(respuestaResolveBuscar.mensaje, respuestaResolveBuscar.usuarioEncontrado)
                        metodosMenu.ejecutarMenuPromesas()
                    })
                    .catch(respuestaRejectBuscar => {
                        console.log(respuestaRejectBuscar.mensaje)
                        metodosMenu.ejecutarMenuPromesas()
                    })

                break;
            case 'Buscar y Crear Un usuario':

                metodosMenu.metodosPromesas.buscarCrearUsuario(arregloUsuarios, ejecutarConsola())
                    .then(resultadoResolveaBuscarCrear => {
                        console.log(resultadoResolveaBuscarCrear.mensaje)
                        metodosMenu.ejecutarMenuPromesas()

                    })
                    .catch(resultadoRejectBuscarCrear => {
                        console.log(resultadoRejectBuscarCrear.mensaje, resultadoRejectBuscarCrear.arregloUsuarios)
                        metodosMenu.ejecutarMenuPromesas()
                    })

            case 'Eliminar Usuario':
                metodosMenu.metodosPromesas.eliminarUsuario(arregloUsuarios, ejecutarConsola())
                    .then(resultadoPromesaEliminar => {
                        console.log(resultadoPromesaEliminar.mensaje, resultadoPromesaEliminar.arregloConUsuarioEliminado)
                        metodosMenu.ejecutarMenuPromesas()

                    })
                    .catch(resultadoCatchEliminar => {
                        console.log(resultadoCatchEliminar.mensaje)
                        metodosMenu.ejecutarMenuPromesas()

                    })
                break;
            case 'Buscar todos los Usuarios':
                const expresion = metodosMenu.readlineSync.question('Ingrese expresion ');
                metodosMenu.metodosPromesas.buscarTodos(arregloUsuarios, expresion ).then(resultadoResolveBuscarTodos => {
                    
                    console.log(resultadoResolveBuscarTodos.arregloUsuariosEncontrados)
                    metodosMenu.ejecutarMenuPromesas()
                })
                    .catch(resultadoRejectBuscarTodos => {
                        console.log(resultadoRejectBuscarTodos.mensaje)
                        metodosMenu.ejecutarMenuPromesas()
                    })
                break

            case 'Salir':
                metodosMenu.ejecutarMenuPrincipal()
                break
            default:
                console.log('Opcion invalida');
        }
    })

}