const fs = require('fs')

module.exports = (path, mensaje) =>{

return new Promise((resolve, reject)=>{
    fs.write(path, mensaje, (errorAlGuardar)=>{
        if (errorAlGuardar){
            reject({
                mensaje: 'hubo un error al guardar',
                error: errorAlGuardar

            })
        }
        else{
            resolve({
                mensaje: 'se creo corretamente',
                error: errorAlGuardar
            })
        }
    });
})
   

}