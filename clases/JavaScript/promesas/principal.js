const leerArchivo = require('./promesas.js')
const escribirArchivo = require('./escribirArchivo.js')

//then entrega resolve  el  catch devuelve reject
leerArchivo('texto1.txt')
.then(resultadoPromesa=>{
    console.log('resultado promesa', resultadoPromesa)
    return escribirArchivo('otroArchivo.txt', 'contenido nuevo')

})
.then(resultadoPromesaCrear =>{
    console.log('resultado promesa crear', resultadoPromesaCrear)
    //return escribirArchivo('otroArchivo.txt','contenido  2nuevo ')

})
// .then(resultadoPromesaCreadoLeido =>{
//     console.log('resultado promesa creado leido ', resultadoPromesaCreadoLeido)

// })
.catch(errorPromesa=>{
    console.log(errorPromesa)
})
