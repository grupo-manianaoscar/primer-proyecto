const fs = require('fs')

module.exports = (path,infoCreada) =>{
return new Promise((resolve, reject)=>{
    fs.readFile(path, 'utf8', (errorAlLeer, contenido)=>{
        if (errorAlLeer){
            reject({
                mensaje: 'hubo un error al leer',
                error: errorAlLeer

            })
        }
        else{
            resolve({
                mensaje: 'se leyo corretamente',
                error: errorAlLeer,
                contenido
            })
        }
    });
})
   

}