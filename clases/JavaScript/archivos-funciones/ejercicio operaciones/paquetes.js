const sumar = require('./sumar.js')
const restar = require('./restar.js')
const multiplicar = require('./multiplicar.js')

module.exports = {
    sumar,
    restar,
    multiplicar
}