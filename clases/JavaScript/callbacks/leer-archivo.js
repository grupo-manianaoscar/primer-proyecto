const fs = require('fs')
module.exports = (path,infoCreada, cb) =>{
    fs.readFile(path, 'utf8', (error, informacion)=>{
        if (error){
            
            fs.writeFile('texto1.txt', infoCreada,(errorAlCrearArchivo)=>{
                
              if(errorAlCrearArchivo){
                  cb({
                    errorAlCrearArchivo:errorAlCrearArchivo,
                    info: infoCreada,
                    crearArchivo: false
                  })

              }
              else{
                cb({
                    errorAlCrearArchivo:errorAlCrearArchivo,
                    info: infoCreada,
                    crearArchivo: true
                  })
              }
                
            })
            
        }
        else{

            cb({//enviar simepre el callback (cb()) para terminar la funcion
                error: error,
                info: informacion,
                crearArchivo: false
            })
        }
    });

}