module.exports = (arregloUsuarios, expresionABuscar, callback)=>{

    let arregloUsuariosEncontrados = arregloUsuarios.map((usuario)=>{
        let encontroConicidenciaNombre = usuario.nombre.toLowerCase().search(expresionABuscar.toLowerCase()) != -1
        if(encontroConicidenciaNombre ){
            return usuario
        }
    }).filter((usuario)=>{
        return usuario != undefined
    })
    
    if (arregloUsuariosEncontrados.length === 0){
        callback({
            mensaje: `no se ha encontrado ningun usuario con la  expresion ${expresionABuscar}`,
            arregloUsuariosEncontrados
        })
    
    }
    else{
        callback({
            mensaje: `se ha encontrado los siguientes usuarios con la  expresion "${expresionABuscar}"`,
            arregloUsuariosEncontrados
        })
    }
}