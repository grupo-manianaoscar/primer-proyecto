const metodos = require('./paquetes.js')
const datosPrueba = require('./datosPrueba.js')

metodos.crearUsuario(datosPrueba.arregloUsuarios, datosPrueba.usuarioNuevo, (resultado)=>{
    console.log(resultado.mensaje)
})
metodos.eliminarUsuario(datosPrueba.arregloUsuarios, datosPrueba.usuarioExistente, (resultado) => {
    console.log(resultado.mensaje)
})

metodos.buscarUsuario(datosPrueba.arregloUsuarios, datosPrueba.usuarioExistente, (resultadoCallbackBuscar) => {
    console.log(resultadoCallbackBuscar.mensaje, resultadoCallbackBuscar.usuarioEncontrado)
})
metodos.buscarPorExpresionRegular(datosPrueba.arregloUsuarios, 'on', (resultado) => {
    console.log(resultado.mensaje)
    console.log(resultado.arregloUsuariosEncontrados)

})

metodos.buscarCrearUsuario(datosPrueba.arregloUsuarios, datosPrueba.usuarioNuevo, (resultado) =>{
    console.log(resultado.mensaje)
})




