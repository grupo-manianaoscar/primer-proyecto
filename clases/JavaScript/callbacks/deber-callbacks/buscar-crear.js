module.exports =(arregloUsuario, usuarioBuscarCrear='', callback )=>{
    const metodos= require('./paquetes.js')
    metodos.buscarUsuario(arregloUsuario, usuarioBuscarCrear, (resultado) =>{
        if(resultado.arregloUsuariosEncontrados.length !== 0){
            callback({
                mensaje: resultado.mensaje
            })
        }
        else {
            metodos.crearUsuario(arregloUsuario, usuarioBuscarCrear, (resultado)=> {
                callback({
                    mensaje: resultado.mensaje,
                    arregloUsuario
                })
            })
            
        }
    })
}