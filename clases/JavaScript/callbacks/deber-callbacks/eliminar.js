module.exports = (arregloUsuarios, usuarioAEliminar='', callback)=>{
    const metodos= require('./paquetes.js')
    metodos.buscarUsuario(arregloUsuarios, usuarioAEliminar, (resultado)=>{
        if(resultado.posicionUsuario != -1){
            arregloUsuarios.splice(resultado.posicionUsuario, 1)
            callback({
                mensaje: `èl usuario ${usuarioAEliminar} fue eliminado`
            })
        }
        else {

            callback({
                mensaje: `${resultado.mensaje} por lo que no pudo ser eliminado`
            })
        }
    } )
    
}