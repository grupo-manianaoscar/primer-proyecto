var parrafo = 'Unos 250 venezolanos que ingresaron de irregular a; Ecuador se beneficiaron el miércoles de un plan de libre movilidad; que permite su traslado directo y gratuito a la frontera con el Perú, a días de que Lima endurezca sus requisitos migratorios. Cuatro autobuses partieron desde el norte de Ecuador con destino al Puente Internacional de Huaquillas, en la frontera con Perú; en un trayecto directo que les ahorra tiempo y dinero, como parte de un plan humanitario implementado por el gobierno local de la provincia de Pichincha. Dos buses más se sumaron en Quito al recorrido, que usualmente toma varios días de punta a punta del país -unos 840 kilómetros- por los múltiples trasbordos y los tramos a pie que se deben recorrer hasta llegar a territorio peruano. Los venezolanos tienen premura de llegar a Perú antes del sábado, cuando el gobierno de ese país comenzará a aplicar la regulación de pasaporte obligatorio para su ingreso. Ecuador impuso una medida similar el 18 de agosto, por lo que los venezolanos que no tienen un documento válido comenzaron a ingresar al país de forma irregular. Ecuador impuso una medida similar el 18 de agosto, por lo que los venezolanos que no tienen un documento válido comenzaron a ingresar al país de forma irregular.'


function cambiarPalabrasMayusculas(parrafo){
  //  let parrafoClonado = parrafo
    let parrafoNUevo =  parrafo.replace(parrafo.substring(0,parrafo.indexOf(' ')),parrafo.substring(0,parrafo.indexOf(' ')).toUpperCase() )
    let ultimaPalabra= parrafoNUevo.slice(parrafoNUevo.lastIndexOf(" "), parrafoNUevo.length )
    parrafoNUevo= `${parrafoNUevo.slice(0,parrafoNUevo.lastIndexOf(" "))}${ultimaPalabra.toUpperCase()}`
    return parrafoNUevo
}


function guardarOracines (){
    let inicio = 0;
    let posicionPunto=  parrafo.indexOf('.')
    console.log('')


    while ( posicionPunto != parrafo.length-1){

        let oracion = parrafo.substring(inicio, posicionPunto);
        console.log(oracion.trim())
        console.log('')
        inicio= posicionPunto+1;
        posicionPunto= parrafo.indexOf('.', inicio)
    }

    

}



function ObtenerNumeroPuntoComas (){

    var caracterAComparar = ''
    var cantidadPuntoComa=0;
    var i=0;

    while (i<=parrafo.length){
        caracterAComparar = parrafo.substring(i, i+1)
        let esPuntoyComa = caracterAComparar == ";"

        if (esPuntoyComa){
            cantidadPuntoComa ++;
        }
        i++
}

console.log('cantidad de ; es ', cantidadPuntoComa)

}


function RemplazarEspacioBlanco (parrafo){
    let i=0;
    let parrafoNuevo=parrafo

    while (i<=parrafoNuevo.length){
        let caracterAComparar = parrafoNuevo.substring(i, i+1)
        let esEspacioEnBlanco = caracterAComparar == " "

        if (esEspacioEnBlanco){
            parrafoNuevo = parrafoNuevo.replace(parrafoNuevo.substring(i, i+1),'*-*')        
        }
        i++
    }
    console.log(parrafoNuevo)
}

// console.log('parrafo modificado: ', cambiarPalabrasMayusculas(parrafo))
// guardarOracines()
// ObtenerNumeroPuntoComas();
RemplazarEspacioBlanco(parrafo)