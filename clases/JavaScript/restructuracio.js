const persona=[{
    nombre: 'oscar',
    apellido: 'sambache',
    edad: 23
},
{
    nombre: 'andres',
    apellido: 'lopez',
    edad: 16
}]

const {nombre, apellido} = persona
const atributosJson = Object.keys(persona)
console.log('atributos objetos json', atributosJson) // listar los atributos

Object.defineProperty(persona, 'nombre', {value: 'oscar sambache'})
console.log(persona)

atributosJson.forEach((valor)=>{
    const esAtributoNombres=valor === 'nombre'
    if(esAtributoNombres){
        Object.defineProperty(persona, valor, {value: 'no tiene'})
    }
})

console.log(persona)

function sumar (a=0,b=0){
    // if(typeof a == 'undefined'){
    //     a=0
    // }
    // if(typeof b == 'undefined'){
    //     b=0
    // }
    // return a|0 + b|0;
    return a+b
}

console.log(sumar(3))
