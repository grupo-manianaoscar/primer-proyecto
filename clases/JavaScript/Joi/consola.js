//npm i console-menu
//https://www.npmjs.com/package/readline-sync

var readlineSync = require('readline-sync');
var menu = require('console-menu');
const arregloUsuario = [];
const arregloejemplo = [[nombre='g', materias=[]], [nombre='gfsdg', materias=[]]]


const arregloSemestre = ["primero", "quinto", "octavo"];
const arregloMateriasPrimero = ["Programacion 1", "Fisica 1", "Calculo 1"];
const arregloMateriasQuinto = ["TCP", "Bases de datos Distribuidas", "Ing Software 1"];
const arregloMateriasOctavo = ["Gestion TIC's", "Planes de negocio", "Auditoria de sistemas"];

const configurarMenu = (header, border) => {
    return {
        header: header,
        border: border,
    }
}

const registrarUsuario = function () {
    var nombreUsuario=readlineSync.question('Ingrese su nombre ');

    arregloUsuario.push(nombreUsuario);
    const hayUsuarios = arregloUsuario.length > 0

    if (hayUsuarios) {
        ejecutarMenuSemestres(nombreUsuario, arregloSemestre)
    } else {
        ejecutarMenu()
    }
}

const logearUsuario = function (usuarios) {
    ejecutarMenuUsuarios(usuarios);

}
const ejecutarMenu = (agregarOpcion) => {

    const menuSeleccion = [
        { hotkey: '1', title: 'Crear usuario' },
    ]
    let existenUsuarios = arregloUsuario.length > 0
    if (existenUsuarios) {
        menuSeleccion[1] = { hotkey: '2', title: 'Logear usuario' }
    }

    return menu(menuSeleccion, configurarMenu('menu principal', true))
        .then(item => {
            switch (item.hotkey) {
                case '1':
                    registrarUsuario();
                    break;
                case '2':
                    logearUsuario(arregloUsuario);
                    break;
                default:
                    console.log('Opcion invalida');
            }
        });
}

const ejecutarMenuSemestres = (usuario, semestres) => {
    let menuSeleccionSemestre = [{ hotkey: '1', title: 'Crear Semestre'}]
    let haySemestres = arregloSemestre.length>0
    if (haySemestres){
        menuSeleccionSemestre[1] = { hotkey: '2', title: 'Listar Semestres'}
    }
    menuSeleccionSemestre.push({ hotkey: `${menuSeleccionSemestre.length + 1}`, title: 'salir' })
    return menu(menuSeleccionSemestre, configurarMenu('Menu Semestres', true)).then(item => {
        let seleccionoSalir = item.title == 'salir'
        if (seleccionoSalir) {
            ejecutarMenu();
        }
        else {
            switch (item.title) {
                case 'Listar Semestres':
                    listarSemestres();
                    break;

                case 'Crear Semestre':
                    crearSemestre(usuario);
                // case '1':
                //     ejecutarMenuMaterias(usuario, item.title, arregloMateriasPrimero);
                //     break;
                // case '2':
                //     ejecutarMenuMaterias(usuario, item.title, arregloMateriasQuinto);
                //     break;
                // case '3':
                //     ejecutarMenuMaterias(usuario, item.title, arregloMateriasOctavo);
                //     break;
                // default:
                //     console.log('Opcion invalida');
            }
        }
    });
}

const ejecutarMenuMaterias = (usuario, semestre, materias) => {
    return menu(llenarArregloMenu(materias), configurarMenu('Menu Materias', true)).then(item => {
        console.log(`el usuario: ${usuario} selecciono el ${semestre} semestre y la materia ${item.title} `)
        materias.splice(materias.indexOf(`${item.title}`), 1)
        ejecutarMenuMaterias(usuario, semestre, materias);
    });

}

const ejecutarMenuUsuarios = (usuarios) => {
    return menu(llenarArregloMenu(usuarios), configurarMenu('Menu Usuarios', true)).then(item => {
        ejecutarMenuSemestres(item.title, arregloSemestre);
    }
    );
}

ejecutarMenu();

const llenarArregloMenu = (arreglo) => {
    let opcionesMenu = []
    arreglo.forEach((valor, indice) => {
        let objetoMenu = {
            hotkey: `${indice + 1}`,
            title: valor
        }
        opcionesMenu.push(objetoMenu)
    })
    return opcionesMenu
}

const listarSemestres = ()=>{
    return menu(llenarArregloMenu(arregloSemestre), configurarMenu('Lista Semestres', true)).then(item => {
        
    }
    );
}

const crearSemestre = (usuario)=>{
    let semestre =readlineSync.question('Ingrese semestre ');
    let existeSemestre = arregloSemestre.indexOf(semestre) == -1
    if (existeSemestre){
        arregloSemestre.push(semestre);
        ejecutarMenuSemestres(usuario, arregloSemestre);
    }
    else{
        console.log('semestre ya existe')
        crearSemestre(usuario);
    }
}