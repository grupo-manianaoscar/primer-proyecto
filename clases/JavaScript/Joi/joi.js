//npm i joi

const Joi = require('joi');


const validacionJsonPersona   = Joi.object().keys({
    nombre: Joi.string().alphanum().min(3).max(30).required(),
    apellido: Joi.string().alphanum().min(3).max(30).required(),
    edad: Joi.number().integer().min(18).max(50),
}).with('nombre', 'apellido');

var objetoPersna1 = {
    nombre: 'Juan',
    apellido: 'Lopez'
}

Joi.validate(objetoPersna1, validacionJsonPersona, function (err, value) { 
   console.log(err)
    console.log('valor', value)
    
}); 