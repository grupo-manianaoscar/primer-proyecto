// var d=9        
// var d=9  
// var d=9  
// var d=9  
// var d=9  
// var d=9  
// var d=9  
//ctrl + shift + 7 comnetar en linea

/* var d=9  
var d=9  
var d=9  
var d=9  
var d=9  */ 

//ctrl + shift + a comenttar en bloque

var pikachu = {
	"forms": [
		{
			"url": "https://pokeapi.co/api/v2/pokemon-form/25/",
			"name": "pikachu"
		}
	],
	"abilities": [
		{
			"slot": 3,
			"is_hidden": true,
			"ability": {
				"url": "https://pokeapi.co/api/v2/ability/31/",
				"name": "lightning-rod"
			}
		},
		{
			"slot": 1,
			"is_hidden": false,
			"ability": {
				"url": "https://pokeapi.co/api/v2/ability/9/",
				"name": "static"
			}
		}
	],
	"stats": [
		{
			"stat": {
				"url": "https://pokeapi.co/api/v2/stat/6/",
				"name": "speed"
			},
			"effort": 2,
			"base_stat": 90
		},
		{
			"stat": {
				"url": "https://pokeapi.co/api/v2/stat/5/",
				"name": "special-defense"
			},
			"effort": 0,
			"base_stat": 50
		},
		{
			"stat": {
				"url": "https://pokeapi.co/api/v2/stat/4/",
				"name": "special-attack"
			},
			"effort": 0,
			"base_stat": 50
		},
		{
			"stat": {
				"url": "https://pokeapi.co/api/v2/stat/3/",
				"name": "defense"
			},
			"effort": 0,
			"base_stat": 40
		},
		{
			"stat": {
				"url": "https://pokeapi.co/api/v2/stat/2/",
				"name": "attack"
			},
			"effort": 0,
			"base_stat": 55
		},
		{
			"stat": {
				"url": "https://pokeapi.co/api/v2/stat/1/",
				"name": "hp"
			},
			"effort": 0,
			"base_stat": 35
		}
	],
	"name": "pikachu",
	"weight": 60,
	"moves": [
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/5/",
				"name": "mega-punch"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/6/",
				"name": "pay-day"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/9/",
				"name": "thunder-punch"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 37,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 37,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 20,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 20,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 21,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 21,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 21,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 20,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 20,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 20,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 20,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 20,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 20,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/21/",
				"name": "slam"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/25/",
				"name": "mega-kick"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/29/",
				"name": "headbutt"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/34/",
				"name": "body-slam"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/36/",
				"name": "take-down"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/38/",
				"name": "double-edge"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 5,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 6,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 6,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 5,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 5,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 5,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 6,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 6,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 6,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 6,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 6,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 5,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 6,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/39/",
				"name": "tail-whip"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 5,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 5,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 5,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/45/",
				"name": "growl"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/5/",
						"name": "stadium-surfing-pikachu"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/5/",
						"name": "stadium-surfing-pikachu"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/57/",
				"name": "surf"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/66/",
				"name": "submission"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/68/",
				"name": "counter"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/69/",
				"name": "seismic-toss"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/70/",
				"name": "strength"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 1,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/84/",
				"name": "thunder-shock"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 42,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 42,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 29,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 29,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 29,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/85/",
				"name": "thunderbolt"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 18,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 18,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 13,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 10,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 8,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 8,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 10,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 10,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 10,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 8,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 8,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 8,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 8,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 8,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 10,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 8,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 9,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/86/",
				"name": "thunder-wave"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 58,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 58,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 41,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 41,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 45,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 45,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 45,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 41,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 41,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 41,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 41,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 41,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 41,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 43,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/87/",
				"name": "thunder"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/91/",
				"name": "dig"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/92/",
				"name": "toxic"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 45,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 45,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 37,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 37,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 34,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 34,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 34,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 37,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 33,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/97/",
				"name": "agility"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 10,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 10,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 10,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 13,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 11,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 11,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 13,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 13,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 13,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 11,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 11,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 11,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 11,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 11,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 13,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 11,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 16,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/98/",
				"name": "quick-attack"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/99/",
				"name": "rage"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/102/",
				"name": "mimic"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 23,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 23,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 21,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 21,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 15,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 15,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 18,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 18,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 18,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 15,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 15,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 15,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 15,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 15,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 15,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 21,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/104/",
				"name": "double-team"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/111/",
				"name": "defense-curl"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 53,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 53,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 45,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 45,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 42,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 42,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 42,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 45,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/113/",
				"name": "light-screen"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/115/",
				"name": "reflect"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/117/",
				"name": "bide"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/129/",
				"name": "swift"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/130/",
				"name": "skull-bash"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/148/",
				"name": "flash"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/156/",
				"name": "rest"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/2/",
						"name": "yellow"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/1/",
						"name": "red-blue"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/164/",
				"name": "substitute"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/173/",
				"name": "snore"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/174/",
				"name": "curse"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/182/",
				"name": "protect"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/189/",
				"name": "mud-slap"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/192/",
				"name": "zap-cannon"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/197/",
				"name": "detect"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/203/",
				"name": "endure"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/205/",
				"name": "rollout"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/207/",
				"name": "swagger"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 26,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/209/",
				"name": "spark"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/213/",
				"name": "attract"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/214/",
				"name": "sleep-talk"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/216/",
				"name": "return"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/218/",
				"name": "frustration"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/223/",
				"name": "dynamic-punch"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/231/",
				"name": "iron-tail"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/237/",
				"name": "hidden-power"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/4/",
						"name": "crystal"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/3/",
						"name": "gold-silver"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/240/",
				"name": "rain-dance"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/249/",
				"name": "rock-smash"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/263/",
				"name": "facade"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/264/",
				"name": "focus-punch"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/270/",
				"name": "helping-hand"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/280/",
				"name": "brick-break"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/282/",
				"name": "knock-off"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/290/",
				"name": "secret-power"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/324/",
				"name": "signal-beam"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/343/",
				"name": "covet"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/13/",
						"name": "xd"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/12/",
						"name": "colosseum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/7/",
						"name": "firered-leafgreen"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/6/",
						"name": "emerald"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/5/",
						"name": "ruby-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/351/",
				"name": "shock-wave"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/363/",
				"name": "natural-gift"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 21,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 34,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 34,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 34,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 29,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 21,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 29,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 29,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/364/",
				"name": "feint"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/374/",
				"name": "fling"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/393/",
				"name": "magnet-rise"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 34,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 42,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 42,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 42,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 37,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 34,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 37,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 37,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/435/",
				"name": "discharge"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/445/",
				"name": "captivate"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/447/",
				"name": "grass-knot"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/9/",
						"name": "platinum"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/8/",
						"name": "diamond-pearl"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/10/",
						"name": "heartgold-soulsilver"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/451/",
				"name": "charge-beam"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 13,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 18,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 18,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 18,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 13,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/486/",
				"name": "electro-ball"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/496/",
				"name": "round"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/497/",
				"name": "echoed-voice"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/521/",
				"name": "volt-switch"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/3/",
						"name": "tutor"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/527/",
				"name": "electroweb"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/14/",
						"name": "black-2-white-2"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/11/",
						"name": "black-white"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 50,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/528/",
				"name": "wild-charge"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 7,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 7,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 7,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/589/",
				"name": "play-nice"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/4/",
						"name": "machine"
					},
					"level_learned_at": 0,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/590/",
				"name": "confide"
			}
		},
		{
			"version_group_details": [
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 29,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/16/",
						"name": "omega-ruby-alpha-sapphire"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 23,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/15/",
						"name": "x-y"
					}
				},
				{
					"move_learn_method": {
						"url": "https://pokeapi.co/api/v2/move-learn-method/1/",
						"name": "level-up"
					},
					"level_learned_at": 29,
					"version_group": {
						"url": "https://pokeapi.co/api/v2/version-group/17/",
						"name": "sun-moon"
					}
				}
			],
			"move": {
				"url": "https://pokeapi.co/api/v2/move/609/",
				"name": "nuzzle"
			}
		}
	],
	"sprites": {
		"back_female": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/female/25.png",
		"back_shiny_female": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/female/25.png",
		"back_default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/25.png",
		"front_female": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/female/25.png",
		"front_shiny_female": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/female/25.png",
		"back_shiny": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/25.png",
		"front_default": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png",
		"front_shiny": "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/25.png"
	},
	"held_items": [
		{
			"item": {
				"url": "https://pokeapi.co/api/v2/item/132/",
				"name": "oran-berry"
			},
			"version_details": [
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/18/",
						"name": "white"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/17/",
						"name": "black"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/16/",
						"name": "soulsilver"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/15/",
						"name": "heartgold"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/14/",
						"name": "platinum"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/13/",
						"name": "pearl"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/12/",
						"name": "diamond"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/9/",
						"name": "emerald"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/8/",
						"name": "sapphire"
					},
					"rarity": 50
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/7/",
						"name": "ruby"
					},
					"rarity": 50
				}
			]
		},
		{
			"item": {
				"url": "https://pokeapi.co/api/v2/item/213/",
				"name": "light-ball"
			},
			"version_details": [
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/28/",
						"name": "moon"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/24/",
						"name": "y"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/23/",
						"name": "x"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/22/",
						"name": "white-2"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/21/",
						"name": "black-2"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/18/",
						"name": "white"
					},
					"rarity": 1
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/17/",
						"name": "black"
					},
					"rarity": 1
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/16/",
						"name": "soulsilver"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/15/",
						"name": "heartgold"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/14/",
						"name": "platinum"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/13/",
						"name": "pearl"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/12/",
						"name": "diamond"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/9/",
						"name": "emerald"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/8/",
						"name": "sapphire"
					},
					"rarity": 5
				},
				{
					"version": {
						"url": "https://pokeapi.co/api/v2/version/7/",
						"name": "ruby"
					},
					"rarity": 5
				}
			]
		}
	],
	"location_area_encounters": "/api/v2/pokemon/25/encounters",
	"height": 4,
	"is_default": true,
	"species": {
		"url": "https://pokeapi.co/api/v2/pokemon-species/25/",
		"name": "pikachu"
	},
	"id": 25,
	"order": 35,
	"game_indices": [
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/22/",
				"name": "white-2"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/21/",
				"name": "black-2"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/18/",
				"name": "white"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/17/",
				"name": "black"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/16/",
				"name": "soulsilver"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/15/",
				"name": "heartgold"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/14/",
				"name": "platinum"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/13/",
				"name": "pearl"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/12/",
				"name": "diamond"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/11/",
				"name": "leafgreen"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/10/",
				"name": "firered"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/9/",
				"name": "emerald"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/8/",
				"name": "sapphire"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/7/",
				"name": "ruby"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/6/",
				"name": "crystal"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/5/",
				"name": "silver"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/4/",
				"name": "gold"
			},
			"game_index": 25
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/3/",
				"name": "yellow"
			},
			"game_index": 84
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/2/",
				"name": "blue"
			},
			"game_index": 84
		},
		{
			"version": {
				"url": "https://pokeapi.co/api/v2/version/1/",
				"name": "red"
			},
			"game_index": 84
		}
	],
	"base_experience": 112,
	"types": [
		{
			"slot": 1,
			"type": {
				"url": "https://pokeapi.co/api/v2/type/13/",
				"name": "electric"
			}
		}
	]
}

var tipo8 = {
	"name": "ghost",
	"generation": {
		"url": "https://pokeapi.co/api/v2/generation/1/",
		"name": "generation-i"
	},
	"damage_relations": {
		"half_damage_from": [
			{
				"url": "https://pokeapi.co/api/v2/type/4/",
				"name": "poison"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/7/",
				"name": "bug"
			}
		],
		"no_damage_from": [
			{
				"url": "https://pokeapi.co/api/v2/type/1/",
				"name": "normal"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/2/",
				"name": "fighting"
			}
		],
		"half_damage_to": [
			{
				"url": "https://pokeapi.co/api/v2/type/17/",
				"name": "dark"
			}
		],
		"double_damage_from": [
			{
				"url": "https://pokeapi.co/api/v2/type/8/",
				"name": "ghost"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/17/",
				"name": "dark"
			}
		],
		"no_damage_to": [
			{
				"url": "https://pokeapi.co/api/v2/type/1/",
				"name": "normal"
			}
		],
		"double_damage_to": [
			{
				"url": "https://pokeapi.co/api/v2/type/8/",
				"name": "ghost"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/14/",
				"name": "psychic"
			}
		]
	},
	"game_indices": [
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/1/",
				"name": "generation-i"
			},
			"game_index": 8
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/2/",
				"name": "generation-ii"
			},
			"game_index": 8
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/3/",
				"name": "generation-iii"
			},
			"game_index": 7
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/4/",
				"name": "generation-iv"
			},
			"game_index": 7
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/5/",
				"name": "generation-v"
			},
			"game_index": 7
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/6/",
				"name": "generation-vi"
			},
			"game_index": 7
		}
	],
	"move_damage_class": {
		"url": "https://pokeapi.co/api/v2/move-damage-class/2/",
		"name": "physical"
	},
	"moves": [
		{
			"url": "https://pokeapi.co/api/v2/move/101/",
			"name": "night-shade"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/109/",
			"name": "confuse-ray"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/122/",
			"name": "lick"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/171/",
			"name": "nightmare"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/174/",
			"name": "curse"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/180/",
			"name": "spite"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/194/",
			"name": "destiny-bond"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/247/",
			"name": "shadow-ball"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/288/",
			"name": "grudge"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/310/",
			"name": "astonish"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/325/",
			"name": "shadow-punch"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/421/",
			"name": "shadow-claw"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/425/",
			"name": "shadow-sneak"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/466/",
			"name": "ominous-wind"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/467/",
			"name": "shadow-force"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/506/",
			"name": "hex"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/566/",
			"name": "phantom-force"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/567/",
			"name": "trick-or-treat"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/636/",
			"name": "never-ending-nightmare--physical"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/637/",
			"name": "never-ending-nightmare--special"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/662/",
			"name": "spirit-shackle"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/695/",
			"name": "sinister-arrow-raid"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/699/",
			"name": "soul-stealing-7-star-strike"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/708/",
			"name": "shadow-bone"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/712/",
			"name": "spectral-thief"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/714/",
			"name": "moongeist-beam"
		}
	],
	"pokemon": [
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/92/",
				"name": "gastly"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/93/",
				"name": "haunter"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/94/",
				"name": "gengar"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/200/",
				"name": "misdreavus"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/292/",
				"name": "shedinja"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/302/",
				"name": "sableye"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/353/",
				"name": "shuppet"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/354/",
				"name": "banette"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/355/",
				"name": "duskull"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/356/",
				"name": "dusclops"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/425/",
				"name": "drifloon"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/426/",
				"name": "drifblim"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/429/",
				"name": "mismagius"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/442/",
				"name": "spiritomb"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/477/",
				"name": "dusknoir"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/478/",
				"name": "froslass"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/479/",
				"name": "rotom"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/487/",
				"name": "giratina-altered"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/562/",
				"name": "yamask"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/563/",
				"name": "cofagrigus"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/592/",
				"name": "frillish"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/593/",
				"name": "jellicent"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/607/",
				"name": "litwick"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/608/",
				"name": "lampent"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/609/",
				"name": "chandelure"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/622/",
				"name": "golett"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/623/",
				"name": "golurk"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/679/",
				"name": "honedge"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/680/",
				"name": "doublade"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/681/",
				"name": "aegislash-shield"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/708/",
				"name": "phantump"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/709/",
				"name": "trevenant"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/710/",
				"name": "pumpkaboo-average"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/711/",
				"name": "gourgeist-average"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/720/",
				"name": "hoopa"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/724/",
				"name": "decidueye"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/769/",
				"name": "sandygast"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/770/",
				"name": "palossand"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/778/",
				"name": "mimikyu-disguised"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/781/",
				"name": "dhelmise"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/792/",
				"name": "lunala"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/802/",
				"name": "marshadow"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10007/",
				"name": "giratina-origin"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10026/",
				"name": "aegislash-blade"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10027/",
				"name": "pumpkaboo-small"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10028/",
				"name": "pumpkaboo-large"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10029/",
				"name": "pumpkaboo-super"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10030/",
				"name": "gourgeist-small"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10031/",
				"name": "gourgeist-large"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10032/",
				"name": "gourgeist-super"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10038/",
				"name": "gengar-mega"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10056/",
				"name": "banette-mega"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10066/",
				"name": "sableye-mega"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10115/",
				"name": "marowak-alola"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10125/",
				"name": "oricorio-sensu"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10143/",
				"name": "mimikyu-busted"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10144/",
				"name": "mimikyu-totem-disguised"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10145/",
				"name": "mimikyu-totem-busted"
			}
		}
	],
	"id": 8,
	"names": [
		{
			"name": "ゴースト",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/1/",
				"name": "ja-Hrkt"
			}
		},
		{
			"name": "고스트",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/3/",
				"name": "ko"
			}
		},
		{
			"name": "Spectre",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/5/",
				"name": "fr"
			}
		},
		{
			"name": "Geist",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/6/",
				"name": "de"
			}
		},
		{
			"name": "Fantasma",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/7/",
				"name": "es"
			}
		},
		{
			"name": "Spettro",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/8/",
				"name": "it"
			}
		},
		{
			"name": "Ghost",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/9/",
				"name": "en"
			}
		}
	]
}

var tipo4 = {
	"name": "poison",
	"generation": {
		"url": "https://pokeapi.co/api/v2/generation/1/",
		"name": "generation-i"
	},
	"damage_relations": {
		"half_damage_from": [
			{
				"url": "https://pokeapi.co/api/v2/type/2/",
				"name": "fighting"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/4/",
				"name": "poison"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/7/",
				"name": "bug"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/12/",
				"name": "grass"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/18/",
				"name": "fairy"
			}
		],
		"no_damage_from": [],
		"half_damage_to": [
			{
				"url": "https://pokeapi.co/api/v2/type/4/",
				"name": "poison"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/5/",
				"name": "ground"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/6/",
				"name": "rock"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/8/",
				"name": "ghost"
			}
		],
		"double_damage_from": [
			{
				"url": "https://pokeapi.co/api/v2/type/5/",
				"name": "ground"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/14/",
				"name": "psychic"
			}
		],
		"no_damage_to": [
			{
				"url": "https://pokeapi.co/api/v2/type/9/",
				"name": "steel"
			}
		],
		"double_damage_to": [
			{
				"url": "https://pokeapi.co/api/v2/type/12/",
				"name": "grass"
			},
			{
				"url": "https://pokeapi.co/api/v2/type/18/",
				"name": "fairy"
			}
		]
	},
	"game_indices": [
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/1/",
				"name": "generation-i"
			},
			"game_index": 3
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/2/",
				"name": "generation-ii"
			},
			"game_index": 3
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/3/",
				"name": "generation-iii"
			},
			"game_index": 3
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/4/",
				"name": "generation-iv"
			},
			"game_index": 3
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/5/",
				"name": "generation-v"
			},
			"game_index": 3
		},
		{
			"generation": {
				"url": "https://pokeapi.co/api/v2/generation/6/",
				"name": "generation-vi"
			},
			"game_index": 3
		}
	],
	"move_damage_class": {
		"url": "https://pokeapi.co/api/v2/move-damage-class/2/",
		"name": "physical"
	},
	"moves": [
		{
			"url": "https://pokeapi.co/api/v2/move/40/",
			"name": "poison-sting"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/51/",
			"name": "acid"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/77/",
			"name": "poison-powder"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/92/",
			"name": "toxic"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/123/",
			"name": "smog"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/124/",
			"name": "sludge"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/139/",
			"name": "poison-gas"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/151/",
			"name": "acid-armor"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/188/",
			"name": "sludge-bomb"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/305/",
			"name": "poison-fang"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/342/",
			"name": "poison-tail"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/380/",
			"name": "gastro-acid"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/390/",
			"name": "toxic-spikes"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/398/",
			"name": "poison-jab"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/440/",
			"name": "cross-poison"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/441/",
			"name": "gunk-shot"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/474/",
			"name": "venoshock"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/482/",
			"name": "sludge-wave"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/489/",
			"name": "coil"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/491/",
			"name": "acid-spray"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/499/",
			"name": "clear-smog"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/562/",
			"name": "belch"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/599/",
			"name": "venom-drench"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/628/",
			"name": "acid-downpour--physical"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/629/",
			"name": "acid-downpour--special"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/661/",
			"name": "baneful-bunker"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/672/",
			"name": "toxic-thread"
		},
		{
			"url": "https://pokeapi.co/api/v2/move/685/",
			"name": "purify"
		}
	],
	"pokemon": [
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/1/",
				"name": "bulbasaur"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/2/",
				"name": "ivysaur"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/3/",
				"name": "venusaur"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/13/",
				"name": "weedle"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/14/",
				"name": "kakuna"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/15/",
				"name": "beedrill"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/23/",
				"name": "ekans"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/24/",
				"name": "arbok"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/29/",
				"name": "nidoran-f"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/30/",
				"name": "nidorina"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/31/",
				"name": "nidoqueen"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/32/",
				"name": "nidoran-m"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/33/",
				"name": "nidorino"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/34/",
				"name": "nidoking"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/41/",
				"name": "zubat"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/42/",
				"name": "golbat"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/43/",
				"name": "oddish"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/44/",
				"name": "gloom"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/45/",
				"name": "vileplume"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/48/",
				"name": "venonat"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/49/",
				"name": "venomoth"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/69/",
				"name": "bellsprout"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/70/",
				"name": "weepinbell"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/71/",
				"name": "victreebel"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/72/",
				"name": "tentacool"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/73/",
				"name": "tentacruel"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/88/",
				"name": "grimer"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/89/",
				"name": "muk"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/92/",
				"name": "gastly"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/93/",
				"name": "haunter"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/94/",
				"name": "gengar"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/109/",
				"name": "koffing"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/110/",
				"name": "weezing"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/167/",
				"name": "spinarak"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/168/",
				"name": "ariados"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/169/",
				"name": "crobat"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/211/",
				"name": "qwilfish"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/269/",
				"name": "dustox"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/315/",
				"name": "roselia"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/316/",
				"name": "gulpin"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/317/",
				"name": "swalot"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/336/",
				"name": "seviper"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/406/",
				"name": "budew"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/407/",
				"name": "roserade"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/434/",
				"name": "stunky"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/435/",
				"name": "skuntank"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/451/",
				"name": "skorupi"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/452/",
				"name": "drapion"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/453/",
				"name": "croagunk"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/454/",
				"name": "toxicroak"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/543/",
				"name": "venipede"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/544/",
				"name": "whirlipede"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/545/",
				"name": "scolipede"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/568/",
				"name": "trubbish"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/569/",
				"name": "garbodor"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/590/",
				"name": "foongus"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/591/",
				"name": "amoonguss"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/690/",
				"name": "skrelp"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/691/",
				"name": "dragalge"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/747/",
				"name": "mareanie"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/748/",
				"name": "toxapex"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/757/",
				"name": "salandit"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/758/",
				"name": "salazzle"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/793/",
				"name": "nihilego"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10033/",
				"name": "venusaur-mega"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10038/",
				"name": "gengar-mega"
			}
		},
		{
			"slot": 2,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10090/",
				"name": "beedrill-mega"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10112/",
				"name": "grimer-alola"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10113/",
				"name": "muk-alola"
			}
		},
		{
			"slot": 1,
			"pokemon": {
				"url": "https://pokeapi.co/api/v2/pokemon/10129/",
				"name": "salazzle-totem"
			}
		}
	],
	"id": 4,
	"names": [
		{
			"name": "どく",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/1/",
				"name": "ja-Hrkt"
			}
		},
		{
			"name": "독",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/3/",
				"name": "ko"
			}
		},
		{
			"name": "Poison",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/5/",
				"name": "fr"
			}
		},
		{
			"name": "Gift",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/6/",
				"name": "de"
			}
		},
		{
			"name": "Veneno",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/7/",
				"name": "es"
			}
		},
		{
			"name": "Veleno",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/8/",
				"name": "it"
			}
		},
		{
			"name": "Poison",
			"language": {
				"url": "https://pokeapi.co/api/v2/language/9/",
				"name": "en"
			}
		}
	]
}