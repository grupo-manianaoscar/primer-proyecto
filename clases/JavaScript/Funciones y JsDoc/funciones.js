function mifuncion (){
    return 'mi funcion'
}

// funcion anonima

var a = function (){
    return 'funcion anonima'
}

console.log(a())

//array function

var b = () =>{

    let c=5
    return 'array function'
}
 //npm install -g jsdoc
 // npm install http-server -g
// jsdoc funciones.js



   /**
     * Funcion para sumar
     * @author Oscar Sambache
     * @copyright Manticore-Labs
     * @param {object} parametro1 Puede ser un number, string, array , boolean, json, null, undefined
     * @param {object} parametro2 Puede ser un number, string, array , boolean, json, null, undefined
     * @returns {object} Resultado de la suma del parametro1 y parametro2
     */
    function suma (parametro1, parametro2){
        return parametro1 + parametro2
    }

    /**
     * Funcion para restar
     * @author Oscar Sambache
     * @copyright Manticore-Labs
     * @param {object} parametro1 Puede ser un number, string, array , boolean, json, null, undefined
     * @param {object} parametro2 Puede ser un number, string, array , boolean, json, null, undefined
     * @returns {object} Resultado de la resta del parametro1 y parametro2
     */
    function restar (parametro1, parametro2){
        return parametro1 - parametro2
    }

      /**
     * Funcion para multiplicar
     * @author Oscar Sambache
     * @copyright Manticore-Labs
     * @param {object} parametro1 Puede ser un number, string, array , boolean, json, null, undefined
     * @param {object} parametro2 Puede ser un number, string, array , boolean, json, null, undefined
     * @returns {object} Resultado de la multiplicaciòn del parametro1 y parametro2
     */
    function multiplicar (parametro1, parametro2){
        return parametro1 * parametro2
    }

    /**
     * Funcion para dividir
     * @author Oscar Sambache
     * @copyright Manticore-Labs
     * @param {object} parametro1 Puede ser un number, string, array , boolean, json, null, undefined
     * @param {object} parametro2 Puede ser un number, string, array , boolean, json, null, undefined
     * @returns {object} Resultado de la división del parametro1 y parametro2
     */
    function dividir (parametro1, parametro2){
        return parametro1 / parametro2
    }


var jsonFunciones = {
    sumar: (parametro1, parametro2)=>
    {
        return parametro1 + parametro2
    }
}

var array = [1,2,3]
var array2= ['hola', 'adios']
var json = {
    id: 1,
    nombre: 'juan'
}
var json2 = {
    id: 2,
    nombre: 'juan'
}
console.log('suma numbers: ', jsonFunciones.sumar(2,6))
console.log('suma de strings: ', jsonFunciones.sumar('hola', ' adios'))
console.log('suma de boolenanos: ', jsonFunciones.sumar(false, false))
console.log('suma de nulls: ', jsonFunciones.sumar(null, null))
console.log('suma de undefined: ', jsonFunciones.sumar(undefined, undefined))
console.log('sume de arrays: ', jsonFunciones.sumar(array, array2))
console.log('suma de jsons: ', jsonFunciones.sumar(json, json2))
console.log('suma de number y string: ', jsonFunciones.sumar(2,'cualquier palabra'))
console.log('suma de number y booleano: ', jsonFunciones.sumar(2, true))
console.log('suma de number y null: ', jsonFunciones.sumar(3,null))
console.log('suma de number y undefinded:', jsonFunciones.sumar(3,undefined))
console.log('suma de number y array de numbers: ', jsonFunciones.sumar(array,5))
console.log('suma de number y array de strings: ', jsonFunciones.sumar(4, array2))
console.log('suma de number y json: ', jsonFunciones.sumar(1,json))
console.log('suma de string y booleano', jsonFunciones.sumar('hola ', true))
console.log('suma de string y null: ', jsonFunciones.sumar('hola ',null))
console.log('suma de string y undefinded: ', jsonFunciones.sumar('hola ',undefined))
console.log('suma de string y array de numbers: ', jsonFunciones.sumar(array,'hola'))
console.log('suma de string y array de strings: ', jsonFunciones.sumar('string', array2))
console.log('suma de string y json', jsonFunciones.sumar('hola',json))
console.log('suma de boolean y null: ', jsonFunciones.sumar(false, null))
console.log('suma de boolean y undefinded:', jsonFunciones.sumar(true,undefined))
console.log('suma de boolean y array de numbers: ', jsonFunciones.sumar(array,true))
console.log('suma de boolean y array de strings: ', jsonFunciones.sumar(false, array2))
console.log('suma de booean y json: ', jsonFunciones.sumar(false,json))
console.log('suma de null y undefinded:', jsonFunciones.sumar(null,undefined))
console.log('suma de null y array de numbers: ', jsonFunciones.sumar(array,null))
console.log('suma de null y array de strings: ', jsonFunciones.sumar(null, array2))
console.log('suma de null y json: ', jsonFunciones.sumar(null,json))
console.log('suma de undefined y array de numbers: ', jsonFunciones.sumar(array,undefined))
console.log('suma de undefined y array de strings: ', jsonFunciones.sumar(undefined, array2))
console.log('suma de undefined y json: ', jsonFunciones.sumar(undefined,json))
console.log('suma de array de numbers y array de strings: ', jsonFunciones.sumar(array, array2))
console.log('suma de array de numbers y json: ', jsonFunciones.sumar(array,json))
console.log('suma de array de strings y json: ', jsonFunciones.sumar(array2,json))


