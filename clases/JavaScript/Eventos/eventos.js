const EventEmitter = require('events');
class EntroAlaCasa extends EventEmitter {

}

const entroAlaCasa = new EntroAlaCasa();

entroAlaCasa.on('por la puerta delantera', (nombre)=>{ //la funcion va a dar la respuesta
    console.log(`entro por la puerta delantera ${nombre}`)
}) //activar el evento

//entroAlaCasa.emit('por la puerta delantera', 'oscar') //se inova al evento

class TocarTimbre extends EventEmitter{

}

const tocarTimbre = new TocarTimbre();

tocarTimbre.on('toco timbre', (nombre)=> {
    console.log(`abrir la puerta a  ${nombre}, que esta timbrando`)
} )

tocarTimbre.on('no se puede abrir la puerta', (nombre)=> {
    console.log(`salir abrir la puerta a  ${nombre}, que no se puede abrir la puerta`)
} )

tocarTimbre.emit('toco timbre', 'Oscar')
tocarTimbre.emit('no se puede abrir la puerta', 'Cristian')
