const generarRandomicos = require("random-js")(); 
var usuarios = [
    {nombre: 'Oscar'}, 
    {nombre: 'Juan'},
    {nombre: 'David'},
    {nombre: 'Andres'},
    {nombre: "Johana"},
    {nombre: "Maria"},
    {nombre: 'Ana'},
    {nombre: 'Jose'},
    {nombre: `Karla`},
    {nombre: `Selena`} 
]

usuarios.map(valor=>{
    valor.fechaNacimiento = obtenerFechaNacimiento();
     valor.edad= calcularEdad(valor.fechaNacimiento);
     return valor;
})
console.log(usuarios)
console.log("")

const usuariosMenoresEdad = usuarios.reduce((acumulador, valor)=>{
    var esMenorEdad = valor.edad < 18
    if (esMenorEdad){
        acumulador.push(valor)
    }
    return acumulador
}, [])


console.log('usuarios menores de edad',usuariosMenoresEdad)
console.log("")

const usuariosAdultoJoven = usuarios.reduce((acumulador, valor)=>{
    var esdAdultoJoven = valor.edad > 18 && valor.edad < 55
    if (esdAdultoJoven){
        acumulador.push(valor)
    }
    return acumulador
}, [])

console.log('usuarios adultos joven',usuariosAdultoJoven)
console.log("")

const usuariosTerceraEdad = usuarios.reduce((acumulador, valor)=>{
    var esTerceraEdad =  valor.edad > 55
    if (esTerceraEdad){
        acumulador.push(valor)
    }
    return acumulador
}, [])
console.log('usuarios tercera edad',usuariosTerceraEdad)


function obtenerFechaNacimiento(){
    let anio= generarRandomicos.integer(1950,2018).toString()
    let mes= generarRandomicos.integer(1,12).toString()
    let dia= generarRandomicos.integer(1,31).toString()
    return `${anio}-${mes}-${dia}`
}

function calcularEdad(FechaNacimiento){
    FechaNacimiento = new Date (FechaNacimiento)
    let fechactual = new Date()
    return (fechactual.getFullYear() - FechaNacimiento.getFullYear())
}

