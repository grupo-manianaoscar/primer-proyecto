const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

const crearArchivo = require('./crear-archivo')

const ejecutarPromesa =(numero)=>{
    archivo= {
        nombreFichero: `${numero}.txt`,
        contenido:`contenido del archivo ${numero}`
    }
    
    const promesaObservable$ = from(crearArchivo(archivo))
    return promesaObservable$
}

const observableEjemplo$ = of(1,2,3,4,5)

const respuesta=(respuesta =>{
    console.log('respuesta', respuesta)
})

const errorObservble=(error =>{
    console.log('error', error)
})


 observableEjemplo$
 .pipe(    
     mergeMap(ejecutarPromesa)
 )
 .subscribe(respuesta, errorObservble)