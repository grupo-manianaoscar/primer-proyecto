const fs = require('fs')

module.exports = (archivo) =>{

return new Promise((resolve, reject)=>{
    fs.writeFile(archivo.nombreFichero, archivo.contenido, (errorAlGuardar)=>{
        if (errorAlGuardar){
            reject({
                mensaje: 'hubo un error al guardar',
                error: errorAlGuardar,
                

            })
        }
        else{
            resolve({
                mensaje: `se creo corretamente el archivo ${archivo.nombreFichero}`,
                error: errorAlGuardar
            })
        }
    });
})
   

}