const ejecutarPromesaBuscar = require('./ejecutarPromesaBuscar')
const ejecutarPromesaEliminar = require('./ejecutarPromesaEliminar')
const ejecutarPromesaBuscarTodos = require('./ejecutarPromesaBuscarTodos')
const ejecutarPromesaCrear = require('./ejecutarPromesaCrear.js')
const ejecutarPromesaBuscarCrear= require('./ejecutarPromesaBuscarCrear')

module.exports = {
    ejecutarPromesaBuscarCrear,
    ejecutarPromesaEliminar,
    ejecutarPromesaCrear,
    ejecutarPromesaBuscar,
    ejecutarPromesaBuscarTodos
}