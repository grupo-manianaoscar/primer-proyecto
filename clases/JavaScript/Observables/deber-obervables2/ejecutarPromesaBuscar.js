module.exports = (arregloUsuarios) => {
    const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
    const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');
    const metodos = require('../../promesas/deber-promesas/paquetes')
    let usuarioAEliminar = 'Cercei'
    const promesaEliminarObservable$ = from(metodos.eliminarUsuario(arregloUsuarios, usuarioAEliminar))
    return promesaEliminarObservable$

}