module.exports = (arregloUsuarios) => {
    const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
    const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');
    const metodos = require('../../promesas/deber-promesas/paquetes')
    let usuarioABuscarCrear = ''
    const promesaBuscarCrearObservable$ = from(metodos.buscarCrearUsuario(arregloUsuarios, usuarioABuscarCrear))
    return promesaBuscarCrearObservable$
}