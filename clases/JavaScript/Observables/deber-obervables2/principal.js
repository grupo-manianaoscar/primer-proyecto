const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');
const metodos = require('./paquetes')

const arregloUsuarios = [
    { nombre: 'Jon' },
    { nombre: 'Eddard' },
    { nombre: 'Daenerys' },
    { nombre: 'Tyrion' },
    { nombre: 'Jaime' },
    { nombre: 'Sansa' },
    { nombre: 'Arya' },
    { nombre: 'Cercei' },
    { nombre: 'Rahegar' },
    { nombre: 'Lyanna' }
]
const observableUsuarios$ = of(arregloUsuarios)
const respuesta = (respuesta => {
    console.log('respuesta', respuesta)
})
const errorObservble = (error => {
    console.log('error', error)
})
const cuandoFinaliza = () => {
    console.log('ya finalizo todo')
}

observableUsuarios$
    .pipe(
        mergeMap(metodos.ejecutarPromesaBuscarTodos),

    )
    .subscribe(respuesta, errorObservble, cuandoFinaliza)
