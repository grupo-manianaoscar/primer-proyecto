const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

const arregloUsuarios = [
    { nombre: 'Jon' },
    { nombre: 'Eddard' },
    { nombre: 'Daenerys' },
    { nombre: 'Tyrion' },
    { nombre: 'Jame' },
    { nombre: 'Sansa' },
    { nombre: 'Arya' },
    { nombre: 'Cercei' },
    { nombre: 'Rahegar' },
    { nombre: 'Lyanna' }
]





const observableEjemplo$ = of(
    { nombre: 'Jon' },
    { nombre: 'Eddard' },
    { nombre: 'Daenerys' },
    { nombre: 'Tyrion' },
    { nombre: 'Jame' },
    { nombre: 'Sansa' },
    { nombre: 'Arya' },
    { nombre: 'Cercei' },
    { nombre: 'Rahegar' },
    { nombre: 'Lyanna' }
)


const respuesta = (respuesta => {
    console.log('respuesta', respuesta)
})

const errorObservble = (error => {
    console.log('error', error)
})

const cuandoFinaliza = () => {
    console.log('ya finalizo todo')
}
function sumarUno(numero) {
    return numero + 1
}
function multiplicarPorDos(numero) {
    return numero * 1
}
function filtrarPares(numero) {
    return numero % 2 === 0
}
function verificarNumero(numero) {
    if (typeof numero !== 'number') {
        return parseInt(numero)
    }
    else {
        return numero
    }
}
function promesaNumeroPar(numero) {
    return new Promise((resolve, reject) => {
        if (numero % 2 == 0) {
            resolve(numero)
        }
        else {
            reject(numero)
        }
    })
}

const ejecutarPromesa = (numero) => {
    const observable$ = from(promesaNumeroPar(numero))
    return observable$
}

observableEjemplo$
    .pipe(
        map((valor, indice) => {

            return valor

        })
        //mergeMap(ejecutarPromesa)   
    )
    .subscribe(respuesta, errorObservble, cuandoFinaliza)


