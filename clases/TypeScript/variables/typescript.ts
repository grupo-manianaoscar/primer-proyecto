// //npm install -g typescript
// //tsc nombreDelArchivo.ts


// // const persona: object = {
// //     nombre: 'oscar',
// //     apellido: 'Sambache'
// // }

// // const edad: number = 55;
// // const nombre: string = 'test'
// // let numero: number;
// // const estado: boolean = true
// // const arreglo: Array<number | string | boolean> = [2, 3, 'algo']
// // const arreglo2: (number | string)[] = [2, 6, '']
// // let variableCualquiera: any;

// // function sumar(a?: number, b?: number): number {

// //     let num1 = 4;
// //     let num2 = 5;
// //     if (a) {
// //         num1 = 10
// //     }
// //     if (b) {
// //         num2 = 20
// //     }
// //     return num1 + num2
// // }

// // sumar(8, 9)
// // sumar(8)
// // sumar()

// // //tsc typescript.ts -t es2017
// // class Persona {
// //     // private _nombre: string
// //     // private _apellido: string
// //     public edad: number

// //     constructor(protected nombre?: string, protected apellido?: string) {
// //         this.nombre = nombre;
// //         this.apellido = apellido
// //         this.edad = this.edad
// //     }

// // }

// // class Estudiante extends Persona {

// //     constructor(public nombre: string, public apellido: string, public carrera: string) {
// //         super(nombre, apellido)
// //         this.carrera = this.carrera
// //     }
// // }

// // const nuevaPersona: Persona = new Persona('juan', 'perez')
// // const estudiante: Estudiante = new Estudiante ('juan','lopez','sistemas')
// // console.log('persona', persona)
// // console.log('tipo', typeof nuevaPersona)
// // console.log('otra persona', nuevaPersona)
// // console.log('estudiante', estudiante)

// class Usuariobase {
//     constructor(public nombre, protected apellido, protected edad){
//         this.nombre=this.nombre
//         this.apellido = this.apellido
//         this.edad= this.edad
//     }
// }

// class UsuariobaseGetSet {
//     constructor(protected nombre, protected apellido, protected edad){
//         this.nombre=this.nombre
//         this.apellido = this.apellido
//         this.edad= this.edad
//     }
//     get getNombreUsuario(){
//         return this.nombre
//     }
//     get apellidoUsuario(){
//         return this.apellido
//     }
//      edadUauario(){
//         return this.edad
//     }
//      setNombreUsuario(nombre){
//         this.nombre = nombre

//     }
//      setApellidoUsuario(apellido){
//         this.apellido=apellido
//     }
//      setEdadUsuario(edad){
//         this.edad=edad
//     }
// }


// class UsuarioPoli extends UsuariobaseGetSet {
//     constructor(public nombre, public apellido, public edad, public numeroUnico){
//         super(nombre, apellido, edad)
//         this.numeroUnico = this.numeroUnico
//     }
// }

// class usuarioSupermaxi extends Usuariobase {
//     constructor(public nombre, public apellido, public edad, public salario){
//         super(nombre, apellido, edad)
//         this.salario = this.salario
//     }
// }

// class usuarioPolicia extends Usuariobase{
//     constructor(public nombre, public apellido, public edad, public cargo){
//         super(nombre, apellido, edad)
//         this.cargo = this.cargo
//     }
// }

// const usuarioPoli1= new UsuarioPoli('oscar','lopez',23,455434)
// const usuarioSupermaxi1= new usuarioSupermaxi('carlos','herrera',54, 564.88)
// const usuarioPlocia1= new usuarioPolicia('manuel','perez',33,'capitan')

// console.log('usuario poli', usuarioPoli1.getNombreUsuario, usuarioPoli1.numeroUnico)
// console.log('usuario supemaxi', usuarioSupermaxi1.nombre, usuarioSupermaxi1.salario)
// console.log('usuario policia', usuarioPlocia1.nombre, usuarioPlocia1.cargo)
