export class UsuariobaseGetSet {
     constructor(protected nombre, protected apellido, protected edad){
        this.nombre=this.nombre
        this.apellido = this.apellido
        this.edad= this.edad
    }
    get getNombreUsuario(){
        return this.nombre
    }
    get apellidoUsuario(){
        return this.apellido
    }
    get edadUauario(){
        return this.edad
    }
     setNombreUsuario(nombre){
        this.nombre = nombre

    }
     setApellidoUsuario(apellido){
        this.apellido=apellido
    }
     setEdadUsuario(edad){
        this.edad=edad
    }
}

