import { usuarioInterface } from './usuario-interface';

const usuarioInterface: usuarioInterface ={
    nombre: 'oscar',
    apellido: 'sambache'
}
console.log('antes',usuarioInterface)

usuarioInterface.nombre = 'eduardo'
usuarioInterface.apellido='perez'

console.log('despues',usuarioInterface)

interface cooreroInterfaz {
    correo:string;
    enviarCorreo(): boolean
}

interface direccionInterfaz{
    direccion: string
}

interface conTodo extends cooreroInterfaz, direccionInterfaz, usuarioInterface{
    estaCompleto: boolean
}

class usuarioCualquiera implements conTodo {
    direccion: string;
    estaCompleto: boolean;
    correo: string;
    enviarCorreo(): boolean {
        throw new Error("Method not implemented.");
    }

    nombre: string;    apellido: string;
}
