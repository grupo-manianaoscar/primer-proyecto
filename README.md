# PRIMER PROYECTO
## DESCRIPCION 

Este repositorio contiene archivos de prueba y ejercicios de las capacitaciones de **MANTICORE**


![descripcion de la imagen](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtlgSVeCaxCHtrO2uL-Y5H6XR6q9l33hjxzY2W9tCAMqCnTSdKYg)

## TEMAS DE CLASES
  
CLASE | TEMA
------------ | -------------
MARTES 14 AGOSTO | [git-gitlab](/clases/git-gitlab)
MIERCOLES 15 AGOSTO | [Markdown](/clases/markdown)
JUEVES 16 AGOSTO |  [Issues y Merge](/clases/issues)
VIERNES 17 AGOSTO |  [Issues y Merge](/clases/issues)
LUNES 20 AGOSTO | [JavaScript](/clases/JavaScript/Variables)
MARTES 21 AGOSTO | [JavaScript-Funciones Y jsDoc](/clases/JavaScript/Funciones%20y%20JsDoc)
MIERCOLES 22 AGOSTO | [JavaScript-Bucles y Condicionales - ](/clases/JavaScript/Bucles) [Eventos](/clases/JavaScript/Eventos)
JUEVES 23 AGOSTO | [JavaScript-Strings](/clases/JavaScript/String) 
VIERNES 24 AGOSTO | [JavaScript - Arrays](/clases/JavaScript/Arrays) [Joi - Menu Console](/clases/JavaScript/Joi) 
LUNES 27 AGOSTO | [JavaScript - Operadores ](/clases/JavaScript/Arrays) [Math ](/clases/JavaScript/Math) 
MARTES 28 AGOSTO | [JavaScript - Date y Moments ](/clases/JavaScript/Fechas)
MIERCOLES 29 AGOSTO | [JavaScript - Funciones en archivos ](/clases/JavaScript/archivos-funciones) [Callbacks](/clases/JavaScript/callbacks)
JUEVES 30 AGOSTO | [JavaScript - Promesas](/clases/JavaScript/promesas)
VIERNES 31 AGOSTO | [JavaScript - Observables](/clases/JavaScript/Observables)
LUNES 1 SEPTIEMBRE | [JavaScript - npm](/clases/JavaScript/npm) [Repositorio npm](https://gitlab.com/OscarSambache1/npm.git)
MARTES 2 SEPTIEMBRE | [TypeScript - variables y clases](/clases/TypeScript/variables)
MIERCOLES 3 SEPTIEMBRE | [TypeScript - interfaces-](/clases/TypeScript/interfaces)[Enum y class validator](/clases/TypeScript/enum)
JUEVES 4 SEPTIEMBRE | [Nest-introduccion](/clases/nest)

el autor de este blog es Oscar Sambache, para mas informacion del autor de clic  [aquí](BIOGRAFIA.md)