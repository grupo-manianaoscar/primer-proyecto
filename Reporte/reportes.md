# Permisos de Guest
* Positivo
    * Se puede clonar el proyecto.
    * Puedo crear una rama localmente.
* Negativo
    * No se puede realizar push.
    * Se puede crear issues pero no se puede asignar etiquetas, ni asignaciones, ni fechas.
  

# Permisos de Reporter
* Positivo
    * Se puede crear una rama localmente.
    * Se puede crear y cerrar issues indicando labels, fechas y asignaciones
* Negativo.
    * No se puede subir al repositorio.
    * No se puede realizar push.


# Permisos de Developer
* Positivo
    * Se puede crear una rama y subirla a repositorio.
    * Se puede realizar push desde cualquier rama.
    * Se puede crear y cerrar issues indicando labels, fechas y asignaciones.
* Negativo
    * No se puede merge request entre ramas secundarias.

# Permisos de Maintanier
* Positivo
    * Se puede crear una rama y subirla a repositorio.
    * Ee puede realizar push desde cualquier rama.
    * Se puede crear y cerrar issues indicando labels, fechas y asignaciones.
    * Se puede realizar merge request de cualquier rama.