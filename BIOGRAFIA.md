## BIOGRAFIA


<p align="center">
  <img width="225" height="300" src="foto.png">
</p>

Oscar Sambache es un estudiante de la [Facultad de Ingeniría en Sistemas ](https://fis.epn.edu.ec/) de la [ Escuela Politécnica Nacional ](http://www.epn.edu.ec/), donde cursa el octavo semestre de la carrerra de Ingeniería en Sistemas.

Oscar cuenta con la experiencia académica en el desarrollo de software en varios lenguajes como JAVA, C#, Python y JavaScript. Ademas posee conocimientos basicos en el tema de redes, sistemas operativos y bases de datos

Oscar vive en la ciudad de Quito, disfruta de escuchar música,  ver series de tv, de hacer ejercicio y le de viajar con su familia.

Se puede contactar a Oscar por medio de los siguientes contactos:
* celular: 0992998263
* email: oscar-crs@hotmail.es
* [facebook](https://www.facebook.com/oscar.sambache)



___


